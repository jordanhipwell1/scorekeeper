//
//  ViewController.m
//  ScoreKeeper
//
//  Created by Jordan Hipwell on 1/14/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *player1NameTextField;
@property (nonatomic, strong) UILabel *player1ScoreLabel;
@property (nonatomic, strong) UIStepper *player1Stepper;

@property (nonatomic, strong) UITextField *player2NameTextField;
@property (nonatomic, strong) UILabel *player2ScoreLabel;
@property (nonatomic, strong) UIStepper *player2Stepper;

@property (nonatomic, strong) UIButton *resetButton;

@end

@implementation ViewController

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.tintColor = [UIColor redColor];
    self.view.backgroundColor = [UIColor blackColor];
    
    CGSize viewSize = self.view.frame.size;
    
    //add name fields
    self.player1NameTextField = [self playerNameTextField];
    self.player1NameTextField.frame = CGRectMake(20, viewSize.height / 5, viewSize.width / 2 - 40, 40);
    self.player1NameTextField.text = @"Player 1";
    [self.view addSubview:self.player1NameTextField];
    
    self.player2NameTextField = [self playerNameTextField];
    self.player2NameTextField.frame = CGRectMake(viewSize.width / 2 + 20, self.player1NameTextField.frame.origin.y, self.player1NameTextField.frame.size.width, self.player1NameTextField.frame.size.height);
    self.player2NameTextField.text = @"Player 2";
    [self.view addSubview:self.player2NameTextField];
    
    //add score labels
    self.player1ScoreLabel = [self scoreLabel];
    self.player1ScoreLabel.frame = CGRectMake(viewSize.width / 4 - 50, viewSize.height / 3, 100, 100);
    [self.view addSubview:self.player1ScoreLabel];
    
    self.player2ScoreLabel = [self scoreLabel];
    self.player2ScoreLabel.frame = CGRectMake(viewSize.width - viewSize.width / 4 - 50, self.player1ScoreLabel.frame.origin.y, self.player1ScoreLabel.frame.size.width, self.player1ScoreLabel.frame.size.height);
    [self.view addSubview:self.player2ScoreLabel];
    
    //add player steppers
    self.player1Stepper = [self playerStepper];
    self.player1Stepper.frame = CGRectMake(self.player1ScoreLabel.frame.origin.x, self.player1ScoreLabel.frame.origin.y + self.player1ScoreLabel.frame.size.height + 75, 50, 50);
    [self.view addSubview:self.player1Stepper];
    
    self.player2Stepper = [self playerStepper];
    self.player2Stepper.frame = CGRectMake(self.player2ScoreLabel.frame.origin.x, self.player1Stepper.frame.origin.y, self.player1Stepper.frame.size.width, self.player1Stepper.frame.size.height);
    [self.view addSubview:self.player2Stepper];
    
    //add reset button
    self.resetButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.resetButton setTitle:@"Reset Scores" forState:UIControlStateNormal];
    self.resetButton.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.resetButton sizeToFit];
    [self.resetButton addTarget:self action:@selector(resetButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    self.resetButton.frame = CGRectMake(viewSize.width / 2 - self.resetButton.frame.size.width / 2, viewSize.height - self.resetButton.frame.size.height - 20, self.resetButton.frame.size.width, self.resetButton.frame.size.height);
    [self.view addSubview:self.resetButton];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //dismiss keyboard when user taps Done
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Methods

- (void)playerStepperTouched:(UIStepper *)sender {
    NSString *newScore = [NSString stringWithFormat:@"%.f", sender.value]; //truncate at decimal
    
    if ([sender isEqual:self.player1Stepper]) {
        self.player1ScoreLabel.text = newScore;
    } else if ([sender isEqual:self.player2Stepper]) {
        self.player2ScoreLabel.text = newScore;
    }
}

- (void)resetButtonTouched:(UIButton *)sender {
    self.player1Stepper.value = 0;
    self.player1ScoreLabel.text = [self initialScoreString];
    
    self.player2Stepper.value = 0;
    self.player2ScoreLabel.text = [self initialScoreString];
}

#pragma mark - Helper Methods

- (NSString *)initialScoreString {
    return [NSString stringWithFormat:@"%d", 0];
}

- (UITextField *)playerNameTextField {
    UITextField *textField = [[UITextField alloc] init];
    textField.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.25];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.textAlignment = NSTextAlignmentCenter;
    textField.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.75];
    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    textField.returnKeyType = UIReturnKeyDone;
    textField.delegate = self;
    
    return textField;
}

- (UILabel *)scoreLabel {
    UILabel *label = [[UILabel alloc] init];
    label.text = [self initialScoreString];
    label.font = [UIFont systemFontOfSize:40];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}

- (UIStepper *)playerStepper {
    UIStepper *stepper = [[UIStepper alloc] init];
    stepper.maximumValue = 21;
    [stepper addTarget:self action:@selector(playerStepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    return stepper;
}

@end
